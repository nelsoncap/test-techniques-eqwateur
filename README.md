# Test Techniques Eqwateur

# Consigne
Le product owner te demande de developper un programme qui permet de calculer le montant à facturer à un client d'Ekwateur pour
un mois calendaire.

Ce programme devra gérer 2 types de clients :

A) Les clients Pro, qui ont les propriétés suivantes :
- Reference Client (EKW + 8 caractères numériques)
- N° SIRET
- Raison Sociale
- CA

B) Les particuliers, qui ont les propriétés suivantes :
- Reference Client (EKW + 8 caractères numériques)
- Civilité
- Nom
- Prénom

Un client peut consommer deux types d'énergies :
- Electricité
- Gaz

Chaque énergie est facturée au kWh.
- Pour les particuliers, le prix du kWh est de 0,121 € pour l'électricité et 0,115€ pour le gaz
- Pour les pro, ayant un CA supérieur à 1 000 000 €, le prix du kWh est de 0,114 € pour l'électricité et 0,111€ pour le gaz
- Pour les pro, ayant un CA inférieur à 1 000 000 €, le prix du kWh est de 0,118 € pour l'électricité et 0,113€ pour le gaz


# Constitution du projet

Le projet est structuré comme suit :

- Un package "models" contenant les entités `Client` et `Invoice`.
- Un package "repositories" et un package "services" pour gérer la base de données.
- Un package "batchs" pour mettre à jour la consommation toutes les X unités de temps et le montant de la facture toutes les Y unités de temps, avec la condition X < Y.
- Un package "controllers" pour la partie visualisation.
- Deux classes de test pour les entités `Client` et `Invoice`.

Le programme utilise sur les frameworks Maven, Spring et Hibernate.

Les URLs sont les suivantes :

- Pour obtenir tous les particuliers : http://localhost:8080/client/individuals
- Pour obtenir tous les clients professionnels : http://localhost:8080/client/companies
- Pour obtenir toutes les factures du mois courant : http://localhost:8080/invoice/current
- Pour obtenir toutes les factures pour le client avec l'ID spécifié : http://localhost:8080/client/{id}/invoice
