package fr.eqwateur.testtechniques.utils;

import fr.eqwateur.testtechniques.type.ClientType;
import fr.eqwateur.testtechniques.type.EnergyType;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

public class ComputeHelper {

    public static Date getFirstDateOfCurrentMonth(){
        Calendar calendar = Calendar.getInstance();

        //Obtenir le premier jour du mois courant
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        return calendar.getTime();
    }

    /**
     * Verfie si la date actuelle est celle de la facturation
     * @return
     */
    public static boolean isLastDateOfCurrentMonth(){
        Calendar calendar = Calendar.getInstance();

        //Obtenir le dernier jour du mois courant
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        return calendar.getTime().equals(new Date());
    }

    public static double getAmountInvoice(double consumptionPerKwh, EnergyType energyType, Optional<Long> revenue){
        switch (energyType){
            case ELECTRICITY:
                if (revenue.isEmpty()) {
                    return consumptionPerKwh * EKWConstant.PRICE_KWH_ELE_IND;
                }
                else if(revenue.get() > EKWConstant.SMALL_REVENUE){
                    return consumptionPerKwh * EKWConstant.PRICE_KWH_ELE_BIG_COM;
                }
                else {
                    return consumptionPerKwh * EKWConstant.PRICE_KWH_ELE_SML_COM;
                }
            case GAZ:
                if (revenue.isEmpty()) {
                    return consumptionPerKwh * EKWConstant.PRICE_KWH_GAZ_IND;
                }
                else if (revenue.get() > EKWConstant.SMALL_REVENUE){
                    return consumptionPerKwh * EKWConstant.PRICE_KWH_GAZ_BIG_COM;
                }
                else {
                    return consumptionPerKwh * EKWConstant.PRICE_KWH_GAZ_SML_COM;
                }
            default:
                throw new IllegalArgumentException("Value "+energyType + " Unknown");
        }
    }

    /**
     * Retourne une valeur de consomation en Kwh alléatoire en fonction des revenu du client
     *  pour une periode donnée
     * @param revenue
     * @return
     */
    public static double getRandomlyConsumptionForPeriod(Optional<Long> revenue){
        if(revenue.isEmpty()) {
            return new Random().nextDouble() * 2;
        }
        else if (revenue.get() <= EKWConstant.SMALL_REVENUE){
            return  new Random().nextDouble() * 20;
        }
        else{
            return  new Random().nextDouble() * 200;
        }
    }
}
