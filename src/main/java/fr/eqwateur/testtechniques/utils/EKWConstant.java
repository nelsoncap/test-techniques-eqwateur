package fr.eqwateur.testtechniques.utils;

public interface EKWConstant {

    // url pour afficher les clients
    String URL_CLIENT = "/client";
    String URL_IND = URL_CLIENT + "/individuals";
    String URL_COM = URL_CLIENT + "/companies";

    // Url pour afficher les factures
    String URL_INVOICE = "/invoice";
    String URL_INVOICE_MONTH = URL_INVOICE + "/current";
    String URL_INVOICE_CLIENT = URL_CLIENT + "/{id}" + URL_INVOICE ;

    long SMALL_REVENUE = 1_000_000;

    double PRICE_KWH_ELE_IND = 0.121;
    double PRICE_KWH_GAZ_IND = 0.115;

    double PRICE_KWH_ELE_BIG_COM = 0.114;
    double PRICE_KWH_GAZ_BIG_COM = 0.111;

    double PRICE_KWH_ELE_SML_COM = 0.118;
    double PRICE_KWH_GAZ_SML_COM = 0.113;
}
