package fr.eqwateur.testtechniques.batchs;


import fr.eqwateur.testtechniques.models.Invoice;
import fr.eqwateur.testtechniques.services.InvoiceService;
import fr.eqwateur.testtechniques.utils.ComputeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Component
public class InvoiceBatch {
    private final Logger log = LoggerFactory.getLogger(InvoiceBatch.class);
    @Autowired
    private InvoiceService invoiceService;

    /**
     * Programme qui met à jours la consomation mensuel d'un client tout les 2 secondes
     */
    @Scheduled(fixedRateString = "${consumption.timing}")
    public void updateConsumption(){
        Date currentMonth = ComputeHelper.getFirstDateOfCurrentMonth();
        log.debug("Month  started since " + currentMonth);
        var invoices = invoiceService.getInvoiceByDate(currentMonth);
        for(Invoice invoice :invoices){
            long clientRevenue = invoice.getClient().getRevenue();
            Optional<Long> revenue =   clientRevenue == 0 ? Optional.empty() :Optional.of(clientRevenue);
            double com = invoice.getConsumptionPerKwh() + ComputeHelper.getRandomlyConsumptionForPeriod(revenue);
            invoiceService.updateConsumption(invoice, com);
        }
        log.info("Since "+ currentMonth+" " + invoices.size() + " invoice(s) has been update");
    }

    /**
     * Programme qui met à jours la ajoute le prix de la facture aux client tout les 30 secondes
     */
    @Scheduled(fixedRateString = "${amount.invoice.timing}")
    public void updateAmount(){
//        if(ComputeHelper.isLastDateOfCurrentMonth()) {
        Date currentMonth = ComputeHelper.getFirstDateOfCurrentMonth();
        Calendar calendar = Calendar.getInstance();
        int totalAmount = 0 ;
        var invoices = invoiceService.getInvoiceByDate(currentMonth);
        for (Invoice invoice : invoices) {
            totalAmount += invoiceService.addAmount(invoice, invoice.getClient()).getAmount();
        }
        log.info("For the mouth " + (calendar.get(Calendar.MONTH) +1 ) + " " + invoices.size() +
                " amount of facture invoice(s) has been update for a total amount of " + totalAmount);
//        }
//        else {
//
//            log.info("No invoice update yet because it's not the end of the month");
//        }
    }
}
