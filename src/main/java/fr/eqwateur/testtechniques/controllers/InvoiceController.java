package fr.eqwateur.testtechniques.controllers;

import fr.eqwateur.testtechniques.models.Invoice;
import fr.eqwateur.testtechniques.services.InvoiceService;
import fr.eqwateur.testtechniques.utils.ComputeHelper;
import fr.eqwateur.testtechniques.utils.EKWConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @GetMapping(EKWConstant.URL_INVOICE_MONTH)
    public List<Invoice> getCurrentMonthInvoices(){
        Date currentMonth = ComputeHelper.getFirstDateOfCurrentMonth();

        return invoiceService.getInvoiceByDate(currentMonth);
    }

    @GetMapping(EKWConstant.URL_INVOICE_CLIENT)
    public List<Invoice> getClientInvoices(@PathVariable("id")long id){

        return invoiceService.getInvoiceByClient(id);
    }
}
