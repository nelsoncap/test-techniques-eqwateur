package fr.eqwateur.testtechniques.controllers;

import fr.eqwateur.testtechniques.models.Client;
import fr.eqwateur.testtechniques.services.ClientService;
import fr.eqwateur.testtechniques.utils.EKWConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController {
    @Autowired
    private ClientService clientService;

    @GetMapping(EKWConstant.URL_IND)
    public ResponseEntity<List<Client>> getIndividualClient(){
        return ResponseEntity.ok(clientService.findIndividualClient());

    }

    @GetMapping(EKWConstant.URL_COM)
    public ResponseEntity<List<Client>> getCompanyClient(){
        return ResponseEntity.ok(clientService.findCompanyClient());

    }
}
