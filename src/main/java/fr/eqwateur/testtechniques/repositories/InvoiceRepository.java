package fr.eqwateur.testtechniques.repositories;

import fr.eqwateur.testtechniques.models.Invoice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

    @Query("SELECT i FROM invoice i WHERE i.date >= :date ")
    public List<Invoice> getInvoicesAfterDate(@Param("date") Date date);

    @Query("SELECT i FROM invoice i WHERE i.client.id = :client_id ")
    List<Invoice> getInvoicesByClientId(@Param("client_id") long clientId);
}
