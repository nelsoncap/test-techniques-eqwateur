package fr.eqwateur.testtechniques.repositories;

import fr.eqwateur.testtechniques.models.Client;
import fr.eqwateur.testtechniques.type.ClientType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("SELECT c FROM client c WHERE c.clientType = :clientType")
    List<Client> getClientByType(@Param("clientType") ClientType clientType);
}
