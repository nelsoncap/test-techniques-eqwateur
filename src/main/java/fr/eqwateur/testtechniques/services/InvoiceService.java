package fr.eqwateur.testtechniques.services;

import fr.eqwateur.testtechniques.models.Client;
import fr.eqwateur.testtechniques.models.Invoice;
import fr.eqwateur.testtechniques.repositories.InvoiceRepository;
import fr.eqwateur.testtechniques.utils.ComputeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class InvoiceService {

    @Autowired
    private InvoiceRepository invoiceRepository;

    public void createInvoice(Invoice invoice){
        invoiceRepository.save(invoice);
    }

    /**
     * Obtenir tous les facture aprés la date
     * @param date
     * @return
     */
    public List<Invoice> getInvoiceByDate(Date date){
        return invoiceRepository.getInvoicesAfterDate(date);
    }

    public List<Invoice> getInvoiceByClient(long clientId){
        return invoiceRepository.getInvoicesByClientId(clientId);
    }
    @Transactional
    public void updateConsumption(Invoice invoice, double consumption){
        invoice.setConsumptionPerKwh(consumption);
        //mis à jour à la date courante
        invoice.setDate(new Date());
        invoiceRepository.save(invoice);
    }

    @Transactional
    public Invoice addAmount(Invoice invoice, Client client){
        Optional<Long> revenue = client.getRevenue() == 0 ? Optional.empty(): Optional.of(client.getRevenue());
        double amount = ComputeHelper.getAmountInvoice(invoice.getConsumptionPerKwh(), invoice.getEnergyType(), revenue);
        invoice.setAmount(amount);
        invoice.setDate(new Date());
        invoiceRepository.save(invoice);

        return invoice;
    }
}
