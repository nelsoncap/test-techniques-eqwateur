package fr.eqwateur.testtechniques.services;

import fr.eqwateur.testtechniques.models.Client;
import fr.eqwateur.testtechniques.repositories.ClientRepository;
import fr.eqwateur.testtechniques.type.ClientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public List<Client> findIndividualClient(){
        return clientRepository.getClientByType(ClientType.INDIVIDUAL);
    }

    public List<Client> findCompanyClient(){
        return clientRepository.getClientByType(ClientType.COMPANY);
    }
}
