package fr.eqwateur.testtechniques.models;

import fr.eqwateur.testtechniques.type.ClientType;

import javax.persistence.*;

@Entity(name = "client")
@Table
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "reference")
    private String ref;

    @Column(name = "siret_code")
    private String siretCode;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "revenue", columnDefinition = "int default 0")
    private long revenue;

    @Column(name = "client_type")
    @Enumerated(EnumType.STRING)
    private ClientType clientType;

    @Column(name = "civility")
    private String civility;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    public Client(){}

    public Client(String ref, String siretCode, String companyName, long revenue) {
        this.ref = ref;
        this.siretCode = siretCode;
        this.companyName = companyName;
        this.revenue = revenue;
        this.clientType = ClientType.COMPANY;
    }

    public Client(String ref, String civility, String lastName, String firstName) {
        this.ref = ref;
        this.civility = civility;
        this.lastName = lastName;
        this.firstName = firstName;
        this.clientType = ClientType.INDIVIDUAL;
    }

    public long getId() {
        return id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getSiretCode() {
        return siretCode;
    }

    public void setSiretCode(String siretCode) {
        this.siretCode = siretCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public long getRevenue() {
        return revenue;
    }

    public void setRevenue(long revenue) {
        this.revenue = revenue;
    }

    public ClientType getClientType() {
        return clientType;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    public String getCivility() {
        return civility;
    }

    public void setCivility(String civility) {
        this.civility = civility;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
