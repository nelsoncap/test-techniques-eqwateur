package fr.eqwateur.testtechniques.models;

import fr.eqwateur.testtechniques.type.EnergyType;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "invoice")
@Table
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "energy")
    @Enumerated(EnumType.STRING)
    private EnergyType energyType;

    @Column(name = "date")
    private Date date;

    private double amount;

    @Column(name = "consumption_per_Kwh")
    private double consumptionPerKwh;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    public long getId() {
        return id;
    }

    public void setEnergyType(EnergyType energyType) {
        this.energyType = energyType;
    }

    public EnergyType getEnergyType() {
        return energyType;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getConsumptionPerKwh() {
        return consumptionPerKwh;
    }

    public void setConsumptionPerKwh(double consumptionPerKwh) {
        this.consumptionPerKwh = consumptionPerKwh;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return client;
    }
}
