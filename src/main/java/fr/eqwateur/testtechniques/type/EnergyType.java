package fr.eqwateur.testtechniques.type;

public enum EnergyType {
    GAZ,
    ELECTRICITY;
}
