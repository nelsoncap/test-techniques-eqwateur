package fr.eqwateur.testtechniques.type;

public enum ClientType {
    INDIVIDUAL,
    COMPANY;
}
