package fr.eqwateur.testtechniques.test;

import fr.eqwateur.testtechniques.models.Client;
import fr.eqwateur.testtechniques.models.Invoice;
import fr.eqwateur.testtechniques.repositories.ClientRepository;
import fr.eqwateur.testtechniques.services.InvoiceService;
import fr.eqwateur.testtechniques.type.EnergyType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InvoiceTest {
    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private ClientRepository clientRepository;

    @Test
    @Order(1)
    public void contextLoad(){
        assertThat(invoiceService).isNotNull();
        assertThat(clientRepository).isNotNull();
    }

    @Test
    @Order(2)
    public void getInvoiceAfterDate() throws ParseException {
        Client client1 = new Client("EKW-01234567", "Mr.", "Doe", "John") ;
        Client client2 = new Client("EKW-01234568", "Mrs.", "Doe", "Jane") ;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
        client1 = clientRepository.save(client1);
        client2 = clientRepository.save(client2);
        Date today = new Date();
        Invoice invoice1 = new Invoice();
        invoice1.setEnergyType(EnergyType.ELECTRICITY);
        invoice1.setDate(formatter.parse("15-10-2023"));
        invoice1.setClient(client1);
        Invoice invoice2 = new Invoice();
        invoice2.setEnergyType(EnergyType.GAZ);
        invoice2.setDate(formatter.parse("25-10-2023"));
        invoice2.setClient(client2);
        Invoice invoice3 = new Invoice();
        invoice3.setEnergyType(EnergyType.ELECTRICITY);
        invoice3.setDate(formatter.parse("25-06-2023"));
        invoice3.setClient(client2);
        invoiceService.createInvoice(invoice1);
        invoiceService.createInvoice(invoice2);
        invoiceService.createInvoice(invoice3);

        List<Invoice> invoices = invoiceService.getInvoiceByDate(today);
        assertThat(invoices.size()).isEqualTo(2);

        List<Invoice> invoicesClient2= invoiceService.getInvoiceByClient(client2.getId());
        assertThat(invoicesClient2.size()).isEqualTo(2);
    }

    @Test
    @Order(3)
    public void updateConsumption() throws ParseException {
        Client client1 = new Client("EKW-01234569", "2565", "Company", 100000) ;
        Client client2 = new Client("EKW-01234570", "2567", "Big Company", 20_000_000) ;
        client1 = clientRepository.save(client1);
        client2 = clientRepository.save(client2);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
        Date today = new Date();
        Invoice invoice1 = new Invoice();
        invoice1.setEnergyType(EnergyType.GAZ);
        invoice1.setDate(formatter.parse("31-10-2023"));
        invoice1.setClient(client1);
        Invoice invoice2 = new Invoice();
        invoice2.setEnergyType(EnergyType.GAZ);
        invoice2.setDate(formatter.parse("20-10-2023"));
        invoice2.setClient(client2);
        invoiceService.createInvoice(invoice1);
        invoiceService.createInvoice(invoice2);
        List<Invoice> invoices = invoiceService.getInvoiceByDate(today);
        assertThat(invoices.size()).isEqualTo(4);
        for(Invoice invoice : invoices){
            invoiceService.updateConsumption(invoice, 100);
        }

        for(Invoice invoice: invoiceService.getInvoiceByDate(today)){
            assertThat(invoice.getConsumptionPerKwh()).isEqualTo(100.0);
        }


        List<Invoice> updated_invoices = new ArrayList<>();
        for(Invoice invoice : invoices){
            updated_invoices.add(
                    invoiceService.addAmount(invoice, invoice.getClient()));
        }
        // client particuler pour electricite
        assertThat(updated_invoices.get(0).getAmount()).isEqualTo(100 * 0.121);
        // Client particuleir pour le gaz
        assertThat(updated_invoices.get(1).getAmount()).isEqualTo(100 * 0.115);
        // Petit entreprise pour le gaz
        assertThat(updated_invoices.get(2).getAmount()).isEqualTo(100 * 0.113);
        // Grande entreprise pour le gaz
        assertThat(updated_invoices.get(3).getAmount()).isEqualTo(100 * 0.111);

    }
}
