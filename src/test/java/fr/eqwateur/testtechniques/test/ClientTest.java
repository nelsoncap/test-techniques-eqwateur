package fr.eqwateur.testtechniques.test;

import fr.eqwateur.testtechniques.models.Client;
import fr.eqwateur.testtechniques.repositories.ClientRepository;
import fr.eqwateur.testtechniques.type.ClientType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClientTest {
    @Autowired
    private ClientRepository repository;

    @Test
    @Order(1)
    public void contextLoad(){
        assertThat(repository).isNotNull();
    }

    @Test
    @Order(2)
    public void createIndividualClient(){
        // recupere les donnée de data.sql 2 company et 2 individual
        Client client = new Client("EKW-01234567", "Mr.", "Doe", "John") ;
        repository.save(client);

        assertThat(repository.getClientByType(ClientType.INDIVIDUAL).size()).isEqualTo(3);
        assertThat(repository.getClientByType(ClientType.COMPANY).size()).isEqualTo(2);
    }

    @Test
    @Order(3)
    public void createCompanyClient(){
        // recupere les donnée de data.sql 2 company et 2 individual
        Client client = new Client("EKW-01234567", "2176", "Company", 1_000) ;
        repository.save(client);

        assertThat(repository.getClientByType(ClientType.INDIVIDUAL).size()).isEqualTo(3);
        assertThat(repository.getClientByType(ClientType.COMPANY).size()).isEqualTo(3);
    }
}
